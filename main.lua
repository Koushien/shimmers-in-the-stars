require 'src/gamescreen'
require 'src/playfield'
require 'src/player'
require 'src/ship'

local gamescreen = Gamescreen()
local window_width, window_height = love.graphics.getDimensions()

function love.load()
  love.graphics.setBackgroundColor(0,0,0,0)

  local p1field = Playfield()
  gamescreen:setPlayfield(p1field, 1)

  local p1 = Player()
  local p1ship = Ship:new(window_width * 0.25, window_height * 0.75)
  p1:setShip(p1ship)
  p1field:setPlayer(p1)
end

function love.update(dt)
  gamescreen:update(dt)
end

function love.draw()
  gamescreen:draw()
end

function love.keypressed(k, sc)
  if k == 'd' then debug.debug() end
end
