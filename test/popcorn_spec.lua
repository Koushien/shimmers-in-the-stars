package.preload.love = package.loadlib('/usr/lib64/liblove.so', 'luaopen_love')
require 'love'
require 'love.filesystem'
require 'love.window'
require 'love.graphics' -- do this before making window to create null shader
require 'love.image'
require 'love.keyboard'
love.window.setMode(3*10^3,3*10^3)

require 'src/popcorn'

describe("#Enemy #constructors", function()
  describe("#Popcorn", function()
    setup(function()
      window_width, window_height = love.graphics.getDimensions()
    end)

    teardown(function()
      window_width, window_height = nil, nil
    end)

    before_each(function()
      popcorn_xPos, popcorn_yPos = math.random(10^3), math.random(10^3)
    end)

    after_each(function()
      popcorn_xPos, popcorn_yPos = nil, nil
    end)

    randomize()

    describe("#small", function()
      it("two args", function()
        local popcorn = Popcorn['small'](popcorn_xPos, popcorn_yPos)
        local comparison_popcorn = Popcorn['small'](popcorn_xPos, popcorn_yPos)
        local comparison_hitbox = comparison_popcorn.hitbox
        local health = popcorn.health
        assert.is_not_nil(popcorn)
        assert.are_equal(true, popcorn.alive)
        assert.are_equal(comparison_hitbox:center(), popcorn.hitbox:center())
        popcorn.collidesWith.bullet(popcorn, Bullet.damage(health - 1)())
        assert.are_equal(1, popcorn.health)
        popcorn.collidesWith.bullet(popcorn, Bullet.damage(1)())
        assert.are_equal(false, popcorn.alive)
        assert.are_equal(true, comparison_popcorn.alive)
      end)
    end)

    describe("#medium", function()
      it("two args", function()
        local popcorn = Popcorn['medium'](popcorn_xPos, popcorn_yPos)
        local comparison_popcorn = Popcorn['medium'](popcorn_xPos, popcorn_yPos)
        local comparison_hitbox = comparison_popcorn.hitbox
        local health = popcorn.health
        assert.is_not_nil(popcorn)
        assert.are_equal(true, popcorn.alive)
        assert.are_equal(comparison_hitbox:center(), popcorn.hitbox:center())
        popcorn.collidesWith.bullet(popcorn, Bullet.damage(health - 1)())
        assert.are_equal(1, popcorn.health)
        popcorn.collidesWith.bullet(popcorn, Bullet.damage(1)())
        assert.are_equal(false, popcorn.alive)
        assert.are_equal(true, comparison_popcorn.alive)
      end)
    end)

    describe("#large", function()
      it("two args", function()
        local popcorn = Popcorn['large'](popcorn_xPos, popcorn_yPos)
        local comparison_popcorn = Popcorn['large'](popcorn_xPos, popcorn_yPos)
        local comparison_hitbox = comparison_popcorn.hitbox
        local health = popcorn.health
        assert.is_not_nil(popcorn)
        assert.are_equal(true, popcorn.alive)
        assert.are_equal(comparison_hitbox:center(), popcorn.hitbox:center())
        popcorn.collidesWith.bullet(popcorn, Bullet.damage(health - 1)())
        assert.are_equal(1, popcorn.health)
        popcorn.collidesWith.bullet(popcorn, Bullet.damage(1)())
        assert.are_equal(false, popcorn.alive)
        assert.are_equal(true, comparison_popcorn.alive)
      end)
    end)
  end)
end)

describe("#Enemy #Explosions", function()
  describe("#Popcorn", function()
    setup(function()
      window_width, window_height = love.graphics.getDimensions()
    end)

    describe("#small", function()
      it("explodes", function()
        local popcorn = Popcorn['small'](0, 0)
        local xPos, yPos = popcorn.xPos, popcorn.yPos
        local radius = math.min(popcorn.width, popcorn.height)
        assert.are_equal("number", type(radius))
        local explosion_spy = spy.on(popcorn, "explosion")
        local explosion = popcorn:explode()
        assert.spy(explosion_spy).was_called_with(xPos, yPos, radius)
      end)
    end)

    describe("#medium", function()
      it("explodes", function()
        local popcorn = Popcorn['medium'](0, 0)
        local xPos, yPos = popcorn.xPos, popcorn.yPos
        local radius = math.min(popcorn.width, popcorn.height)
        assert.are_equal("number", type(radius))
        local explosion_spy = spy.on(popcorn, "explosion")
        local explosion = popcorn:explode()
        assert.spy(explosion_spy).was_called_with(xPos, yPos, radius)
      end)
    end)

    describe("#large", function()
      it("explodes", function()
        local popcorn = Popcorn['large'](0, 0)
        local xPos, yPos = popcorn.xPos, popcorn.yPos
        local radius = math.min(popcorn.width, popcorn.height)
        assert.are_equal("number", type(radius))
        local explosion_spy = spy.on(popcorn, "explosion")
        local explosion = popcorn:explode()
        assert.spy(explosion_spy).was_called_with(xPos, yPos, radius)
      end)
    end)

  end)
end)
