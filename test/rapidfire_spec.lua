package.preload.love = package.loadlib('/usr/lib64/liblove.so', 'luaopen_love')
require 'love'
require 'love.filesystem'
require 'love.window'
require 'love.graphics' -- do this before making window to create null shader
require 'love.image'
love.window.setMode(3*10^3,3*10^3)

require 'src/rapidfire'

describe("#Bullet #constructors", function()
  describe("#RapidFire", function()
    it("two args", function()
      rand_rapidfire = RapidFire(math.random(10^3),math.random(10^3))
      assert.is_not_nil(rand_rapidfire)
      assert.are_equal(true, rand_rapidfire:checkAlive())
      rand_rapidfire = nil
    end)
  end)
end)

describe("#Bullet #update", function()
  setup(function()
    rand_rapidfire = RapidFire(math.random(10^3), math.random(10^3))
  end)

  teardown(function()
    rand_rapidfire = nil
    dt = nil
  end)

  before_each(function()
    dt = math.random()
  end)

  describe("#RapidFire", function()
    it("should stay alive", function()
      assert.are_equal(true, rand_rapidfire.alive)
      rand_rapidfire:update(dt)
      assert.are_equal(true, rand_rapidfire.alive)
    end)
  end)
end)
