package.preload.love = package.loadlib('/usr/lib64/liblove.so', 'luaopen_love')
require 'love'
require 'love.filesystem'
require 'love.window'
require 'love.graphics' -- do this before making window to create null shader
require 'love.image'
require 'love.keyboard'
require 'love.math'
love.window.setMode(3*10^3,3*10^3)
local dlist = require 'include/dlist/dlist'

require 'src/playfield' -- load first, HC collision?
require 'src/pattern'
require 'src/popcorn'
require 'src/rapidfire'

local function random_popcorn_size_helper()
  rng = math.random(10^3)
  if rng % 3 == 0 then return 'small'
  elseif rng % 3 == 1 then return 'medium'
  else return 'large'
  end
end

describe("#Playfield #update", function()
  setup(function()
    playfield = Playfield()
    rand_popcorn_size = random_popcorn_size_helper()
  end)

  teardown(function()
    playfield = nil
  end)

  before_each(function()
    dt = math.random()
    playfield.bullets = dlist:new()
    playfield.enemies = dlist:new()
    rand_xPos, rand_yPos = math.random(10^3), math.random(10^3)
  end)

  after_each(function()
    dt = 0
    rand_xPos, rand_yPos  = nil, nil
  end)

  describe("#bullets", function()
    describe("#RapidFire", function()
      it("adds one dead bullet and checks it is gone after update", function()
        local dead_rapidfire = RapidFire(rand_xPos, rand_yPos)
        dead_rapidfire.alive = false
        playfield.bullets:push(dead_rapidfire)
        assert.are_equals(1, playfield.bullets.length)
        playfield:updateBullets(dt)
        assert.are_equals(0, playfield.bullets.length)
      end)
      it("checks the bullet is where it should be", function()
        local rand_rapidfire = RapidFire(rand_xPos, rand_yPos)
        local comparison_rapidfire = RapidFire(rand_xPos, rand_yPos)
        assert.are_equals(true, rand_rapidfire.alive)
        playfield:addBullet(rand_rapidfire)
        playfield:updateBullets(dt)
        assert.are_equals(true, rand_rapidfire.alive)
        assert.is_not_nil(playfield.bullets:next())
        assert.are_equals(true, playfield.bullets:next().alive)
        assert.are_equals(
            comparison_rapidfire.xPos + comparison_rapidfire.xVel * dt,
            playfield.bullets:next().xPos)
        assert.are_equals(
            comparison_rapidfire.yPos + comparison_rapidfire.yVel * dt,
            playfield.bullets:next().yPos)
      end)
    end)
  end)

  describe("#enemies", function()
    describe("#Popcorn", function()
      describe("#" .. rand_popcorn_size, function()
        it("adds one dead enemy and checks it is gone after update", function()
          local dead_popcorn =
              Popcorn[rand_popcorn_size](rand_xPos, rand_yPos)
          local xPos, yPos = dead_popcorn.xPos, dead_popcorn.yPos
          local scale = math.min(dead_popcorn.width, dead_popcorn.height)
          local explosion_spy = spy.on(Explosion, "init")
          dead_popcorn.alive = false
          playfield.enemies:push(dead_popcorn)
          assert.are_equals(1, playfield.enemies.length)
          playfield:updateEnemies(dt)
          assert.spy(explosion_spy).was_called()
          assert.spy(explosion_spy).was_called_with(
              Explosion, xPos, yPos, scale)
          assert.are_equals(0, playfield.enemies.length)
        end)
      end)
    end)

    end)
  end)
end)

describe("#Playfield #lists", function()
  setup(function()
    playfield = Playfield()
    rand_rapidfire = RapidFire(math.random(10^3), math.random(10^3))
  end)
  teardown(function()
    playfield = nil
    randBullet = nil
    rand_rapidfire = nil
  end)

  after_each(function()
    playfield.bullets:clear()
  end)

  describe("#push", function()
    insulate("", function()
      it("nothing added yet", function()
        assert.are_equals(0, playfield.bullets.length)
      end)
    end)
    insulate("#bullets", function()
      it("#RapidFire", function()
        playfield:addBullet(rand_rapidfire)
        assert.are_equals(1, playfield.bullets.length)
      end)
    end)
  end)

  describe("#update", function()
  end)
end)
