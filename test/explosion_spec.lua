package.preload.love = package.loadlib('/usr/lib64/liblove.so', 'luaopen_love')
require 'love'
require 'love.filesystem'
require 'love.window'
require 'love.graphics' -- do this before making window to create null shader
require 'love.image'
require 'love.keyboard'
require 'love.math'
love.window.setMode(3*10^3,3*10^3)

local dlist = require 'include/dlist/dlist'

require 'src/playfield'
require 'src/popcorn'

local function random_popcorn_size_helper()
  rng = math.random(10^3)
  if rng % 3 == 0 then return 'small'
  elseif rng % 3 == 1 then return 'medium'
  else return 'large'
  end
end

describe("#Explosion does damage", function()
  setup(function()
    playfield = Playfield()
    playfield.player = {hitbox = nil}
    window_width, window_height = love.graphics.getDimensions()
  end)

  teardown(function()
    playfield = nil
    window_width, window_height = nil, nil
  end)

  before_each(function()
    dt = math.random()
    playfield.bullets = dlist:new()
    playfield.enemies = dlist:new()
    rand_xPos, rand_yPos = math.random(10^3), math.random(10^3)
  end)

  after_each(function()
    dt = 0
    rand_xPos, rand_yPos = nil, nil
  end)

  describe("#Playfield", function()
    describe("#Popcorn", function()
      it("blows up a random-sized popcorn, destroying a small one", function()
        local rand_popcorn_size = random_popcorn_size_helper()
        local dead_popcorn = Popcorn[rand_popcorn_size](rand_xPos, rand_yPos)
        dead_popcorn.alive = false
        local small_popcorn = Popcorn['small'](rand_xPos, rand_yPos)
        playfield:addEnemy(dead_popcorn)
        playfield:addEnemy(small_popcorn)
        assert.are_equals(2, playfield.enemies.length)
        playfield:updateEnemies(dt)
        assert.are_equals(1, playfield.enemies.length)
        playfield:checkCollisions(dt)
        assert.are_equals(false, small_popcorn.alive)
        playfield:updateEnemies(dt)
        assert.are_equals(0, playfield.enemies.length)
      end)
      it("blows up a random-sized popcorn, damging one other once", function()
        local rand_popcorn_size = random_popcorn_size_helper()
        local dead_popcorn = Popcorn[rand_popcorn_size](rand_xPos, rand_yPos)
        dead_popcorn.alive = false
        local small_popcorn = Popcorn['small'](rand_xPos, rand_yPos)
        small_popcorn.health = 4
        playfield:addEnemy(dead_popcorn)
        playfield:addEnemy(small_popcorn)
        assert.are_equals(2, playfield.enemies.length)
        playfield:updateEnemies(dt)
        assert.are_equals(1, playfield.bullets.length)
        playfield:checkCollisions(dt)
        assert.are_equals(true, small_popcorn.alive)
        playfield:checkCollisions(dt)
        assert.are_equals(true, small_popcorn.alive)
        playfield:updateEnemies(dt)
        assert.are_equals(1, playfield.enemies.length)
        assert.are_equals(1, small_popcorn.health)
      end)
    end)
  end)
end)
