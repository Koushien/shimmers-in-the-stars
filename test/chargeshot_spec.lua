package.preload.love = package.loadlib('/usr/lib64/liblove.so', 'luaopen_love')
require 'love'
require 'love.filesystem'
require 'love.window'
require 'love.graphics' -- do this before making window to create null shader
require 'love.image'
require 'love.keyboard'
love.window.setMode(3*10^3,3*10^3)

require 'src/chargeshot'

describe("#Bullet #constructors", function()
  describe("#ChargeShot", function()
    before_each(function()
      cshot_xPos, cshot_yPos = math.random(10^3), math.random(10^3)
      window_width, window_height = love.graphics.getDimensions()
    end)

    after_each(function()
      cshot_xPos, cshot_yPos = nil, nil
    end)

    describe("#levelOne", function()
      it("two args", function()
        chargeshot = ChargeShot[1](cshot_xPos, cshot_yPos)
        assert.is_not_nil(chargeshot)
        assert.are_equal(true, chargeshot.alive)
        assert.are_equal(true, chargeshot:checkAlive())
        chargeshot = nil
      end)
    end)

    describe("#levelTwo", function()
      it("two args", function()
        chargeshot = ChargeShot[2](cshot_xPos, cshot_yPos)
        assert.is_not_nil(chargeshot)
        assert.are_equal(true, chargeshot.alive)
        assert.are_equal(true, chargeshot:checkAlive())
        chargeshot = nil
      end)
    end)

    describe("#levelThree", function()
      it("two args", function()
        chargeshot = ChargeShot[3](cshot_xPos, cshot_yPos)
        assert.is_not_nil(chargeshot)
        assert.are_equal(true, chargeshot.alive)
        assert.are_equal(true, chargeshot:checkAlive())
        chargeshot = nil
      end)
    end)

  end)
end)
