package.preload.love = package.loadlib('/usr/lib64/liblove.so', 'luaopen_love')
require 'love'
require 'love.filesystem'
love.filesystem.init(arg[0])
love.filesystem.setSource("/home/ervin/school/neet/shimmers-in-the-stars/")
local seed = math.random()
math.randomseed(seed)

-- check if the log is more than thirty lines long
-- if so, delete the oldest until we have less than thirty
local seed_log = io.open("test/seed_log.txt", "r")
local num_lines = 0
for line in seed_log:lines() do
  num_lines = num_lines + 1
end
seed_log:close()

seed_log = io.open("test/seed_log.txt", "r+")
if num_lines > 30 then
  local diff = (num_lines - 30) or 0
  local i = 0
  for line in seed_log:lines() do
    if i < diff then
      delete = string.gsub(line, line, '', 1)
      seed_log:write(delete)
      i = i + 1
    end
  end
end
seed_log:close()

seed_log = io.open("test/seed_log.txt", "a+")
local date = os.date()
seed_log:seek("end", 1)
seed_log:write("For the test run at " .. date .. " the seed was " .. seed, "\n")
seed_log:close()
