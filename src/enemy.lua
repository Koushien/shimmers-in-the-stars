-- Enemies are represented by an on-screen graphic and an associated width,
-- height for said graphic, have x/y coords and velocities, a specified amount
-- of health, a hitbox and can be directed by commands.
require 'include/HardonCollider.shapes'
require 'include/math/math'

require 'src/explosion'

Enemy = {}
Enemy.__index = Enemy

setmetatable(Enemy, {
  __call = function(cls)
    local self = setmetatable({}, cls)
    self:init()
    return self
    end
})

Enemy = {
  graphic = nil,
  width = 0, height = 0,
  xPos = 0, yPos = 0,
  xVel = 0, yVel = 0,
  health = 0,
  hitbox = nil
}

function Enemy:init()
  self.alive = true
  self.explosion = Explosion
end

function Enemy:explode()
  return self.explosion(self.xPos, self.yPos, math.min(self.width, self.height))
end

function Enemy:movementHelper(x, y)
  self.xPos = x
  self.yPos = y
  self.hitbox:moveTo(x + self.width * 0.5, y + self.height * 0.5)
end

function Enemy:move(dt)
  if self.curve then
    local delta_xPos, delta_yPos = self.xVel * dt, self.yVel * dt
    local delta = math.sqrt(delta_xPos ^ 2 + delta_yPos ^ 2)
    local curve_pos, next_xPos, next_yPos = self.curve:nextPoint(delta)
    if curve_pos >= 1 then
      self.alive = false
      self.explosion = nil
      self.shatter = nil
    else
      self:movementHelper(next_xPos, next_yPos)
    end
  end
end

Enemy.collidesWith = {
  bullet = function(self, effect)
    prev_health = self.health
    effect(self)
    if prev_health > self.health then self.damaged = true end
    if self.health <= 0 then self.alive = false end
  end
}

function Enemy:update(dt)
  if self.alive then
    self:move(dt)
  end
end

function Enemy:drawModulatedGraphic(xOffset,yOffset)
  love.graphics.setColor(255, 255, 255, 255)
  love.graphics.draw(self.graphic, self.xPos + xOffset, self.yPos + yOffset, 0,
                     self.scale, self.scale)
  love.graphics.setBlendMode('add')
  love.graphics.setColor(255 - math.pow(self.health, 5), 0, 0, 255/2)
  love.graphics.draw(self.alpha, self.xPos + xOffset, self.yPos + yOffset, 0,
                     self.scale, self.scale)
  love.graphics.setColor(255, 255, 255)
  love.graphics.setBlendMode('alpha')
end

function Enemy:flash(xOffset, yOffset)
  love.graphics.setColor(255, 0, 0)
  love.graphics.draw(self.alpha, self.xPos + xOffset, self.yPos + yOffset, 0,
                     self.scale, self.scale)
  love.graphics.setColor(255, 255, 255)
  self.damaged = false
end

function Enemy:drawHitbox()
  if self.hitbox then
    love.graphics.setColor(255,255,0)
    self.hitbox:draw('fill')
    love.graphics.setColor(255,255,255)
  end
end

function Enemy:draw(xOffset,yOffset)
  self:drawModulatedGraphic(xOffset, yOffset)
  if self.damaged == true then self:flash(xOffset, yOffset) end
  self:drawHitbox()
end
