-- A bullet is represented by a graphic with a hitbox and x/y coords and
-- velocities. Each bullet does a particular amount of damage on hit and has a
-- maximum distance it can travel.
require 'include/HardonCollider.shapes'

Bullet = {}
Bullet.__index = Bullet

setmetatable(Bullet, {
  __call = function(cls)
    local self = setmetatable({}, cls)
    self:init()
    return self
  end,
})

Bullet = {
  graphic = nil,
  width = 0, height = 0,
  xPos = 0, yPos = 0,
  xVel = 0, yVel = 0,
  damage = function(damage)
             return function()
               return function(enemy)
                 enemy.health = enemy.health - damage or 0
               end
             end
          end,
  range = 0
}

Bullet.collidesWith = {
  enemy = function(self) self.alive = false end
}

function Bullet:init()
  self.alive = true
  self.distance_traveled = 0
  self.damage = Bullet.damage(self.damage)
end

function Bullet:move(dt)
  local delta_xPos, delta_yPos = self.xVel * dt, self.yVel * dt
  self.xPos = self.xPos + delta_xPos
  self.yPos = self.yPos + delta_yPos
  local function move_hitbox(dx, dy) self.hitbox:move(dx, dy) end
  move_hitbox(delta_xPos, delta_yPos)
  self.distance_traveled = self.distance_traveled + math.abs(self.xVel * dt) +
                           math.abs(self.yVel * dt)
end

function Bullet:checkAlive()
  local window_width, window_height = love.graphics.getDimensions()
  if (self.distance_traveled >= self.range) then
    self.alive = false
  elseif (self.xPos < 0 - self.width or
    self.xPos > window_width * 0.5 + self.width) then
    self.alive = false
  elseif (self.yPos < 0 or self.yPos > window_height - self.height) then
    self.alive = false
  end
  return self.alive
end

function Bullet:update(dt)
  if self:checkAlive() then self:move(dt) end
end

function Bullet:drawHitbox()
  if self.hitbox then
    love.graphics.setColor(0,255,0)
    local function draw_hitbox() self.hitbox:draw('fill') end draw_hitbox()
    love.graphics.setColor(255,255,255)
  end
end

function Bullet:draw(xOffset, yOffset)
  love.graphics.draw(self.graphic, self.xPos + xOffset, self.yPos + yOffset)
  -- self:drawHitbox()
end
