require 'src/pattern'

Patterns = {}

local field_length = love.graphics.getWidth() / 2

local A1 = {
  enemies = {
    {
      function() return Popcorn['large'](-5, -5) end,
      function() return Popcorn['medium'](-55, -55) end,
      function() return Popcorn['small'](-105, -105) end,
      function() return Popcorn['small'](-155, -155) end,
      function() return Popcorn['small'](-205, -205) end
    }
  },
  instructions = {
    {
      -- enter from top left of playfield
      1, .33,
      -- move to just below center of playfield
      .5, .55,
      -- sharply move upwards to meet actual center
      .5, .5,
      -- exit to the bottom left of playfield
      0, 1.05
    }
  }
}
table.insert(Patterns, A1)

local A2 = {
  enemies = {
    {
      function() return Popcorn['medium'](field_length / 2 - 100, -25) end,
      function() return Popcorn['small'](field_length / 2 - 50, -5) end
    },
    {
      function() return Popcorn['medium'](field_length / 2, -25) end
    },
    {
      function() return Popcorn['small'](field_length / 2 + 50, -5) end,
      function() return Popcorn['medium'](field_length / 2 + 100, -25) end
    }
  },
  instructions = {
      -- enter from top of playfield
      -- move downwards, slowly convering near the bottom center of the screen
      -- then slowly drift away
    {
      .50, 1.45,
      .00, 1.60,
    },
    {
      .50, 1.45,
      .50, 1.60,
    },
    {
      .50, 1.45,
      1.00, 1.60,
    }
  }
}
table.insert(Patterns, A2)

local A3 = {
  enemies = {
    {
      function() return Popcorn['small'](-5, -5) end,
      function() return Popcorn['large'](-55, -55) end,
      function() return Popcorn['small'](-95, -95) end
    },
    {
      function() return Popcorn['medium'](field_length + 25, -25) end,
      function() return Popcorn['large'](field_length + 75, -75) end
    }
  },
  instructions = {
    -- flat double helix
    {
      -- spawn near top left
      .5, .3,
      1, .6,
      .5, .9,
      -0.05, 1.05
    },
    {
      -- spawn near top right
      .5, .3,
      0, .6,
      .5, .9,
      1.05, 1.05
    }
  }
}
table.insert(Patterns, A3)

local pattern_metatable = {
  __index = setmetatable(Pattern, {__index = Pattern}),
  __call = function(cls)
    local self = setmetatable({}, cls)
    local template = {}
    assert(#(cls.enemies) == #(cls.instructions))
    local max = #(cls.enemies)
    for i=1,max do
      local page = {}
      page.enemies = cls.enemies[i]
      page.instructions = cls.instructions[i]
      table.insert(template, page)
    end
    Pattern.init(self, template)
    return self.enemies
  end
}

local patterns_metatable = {
  __index = function(t, k)
    if type(k) == "number" then
      return t[k % #t]
    else return nil
    end
  end
}

setmetatable(Patterns, {
  __call = function(cls)
    local self = setmetatable({}, patterns_metatable)
      for i,_ in ipairs(Patterns) do
       table.insert(self, setmetatable(Patterns[i], pattern_metatable))
      end
    return self
  end
})
