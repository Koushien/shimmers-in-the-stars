-- A ship is the unit a player controls.
local shapes = require 'include/HardonCollider.shapes'

require 'src/rapidfire'
require 'src/chargeshot'

Ship = {}
Ship.__index = Ship

function Ship:new(x,y)
  local ship = {
    shot = RapidFire,
    chargeshot = nil,
    charge_duration = 0,
    cooldown = 0,
    xVel = 300,
    yVel = 300
  }
  ship.graphic = love.graphics.newImage("res/space-shooter/hud/lives-icon.png")
  ship.width, ship.height = ship.graphic:getDimensions()
  ship.xPos = (x or 0) - ship.width * 0.5
  ship.yPos = (y or 0) - ship.height * 0.5
  ship.hitbox = shapes.newCircleShape(ship.xPos + ship.width * 0.5,
                                      ship.yPos + ship.height * 0.5, 5.5)
  setmetatable(ship, self)
  return ship
end

function Ship:charging(dt)
  if love.keyboard.isDown('x') then
    self.charge_duration = self.charge_duration + dt
    if (self.charge_duration < 3) then self.chargeshot = ChargeShot[1]
    elseif (self.charge_duration < 6) then self.chargeshot = ChargeShot[2]
    else self.chargeshot = ChargeShot[3]
    end
  if self.chargeshot and self.chargeshot.charge_effect then
    self.chargeshot.charge_effect.animation:update(dt)
  end
  end
end

function Ship:fire(dt)
  self.cooldown = self.cooldown + dt
  if love.keyboard.isDown('z') and self.chargeshot == nil
    and self.cooldown > 0.125 then self.cooldown = 0
    return self.shot(self.xPos + self.width * 0.5,
                     self.yPos - self.shot.graphic:getHeight())
  end
end

function Ship:fireChargeShot()
  if not love.keyboard.isDown("x") then
    self.charge_duration = 0
    if self.chargeshot then
      shot = self.chargeshot(self.xPos + self.width * 0.5,
                             self.yPos - self.chargeshot.graphic:getHeight())
      self.chargeshot = nil
    return shot
    end
  end
end

function Ship:moveHitbox()
  self.hitbox:moveTo(self.xPos + self.width * 0.5,
                     self.yPos + self.height * 0.5)
end

function Ship:move(dt)
  local window_width, window_height = love.graphics.getDimensions()
  local delta_xPos = self.xVel * dt
  local delta_yPos = self.yVel * dt
  if (self.charge_duration > 0) then
    delta_xPos = delta_xPos * 0.5
    delta_yPos = delta_yPos * 0.5
  end
  if love.keyboard.isDown("left") and not love.keyboard.isDown("right") then
    if not (self.xPos - delta_xPos < 0) then
      self.xPos = self.xPos - delta_xPos
    end
  elseif love.keyboard.isDown("right") and not love.keyboard.isDown("left") then
    if not (self.xPos + delta_xPos > window_width * 0.5 - self.width) then
      self.xPos = self.xPos + delta_xPos
    end
  end
  if love.keyboard.isDown("up") and not love.keyboard.isDown("down") then
    if not (self.yPos - delta_yPos < 0) then
      self.yPos = self.yPos - delta_yPos
    end
  elseif love.keyboard.isDown("down") and not love.keyboard.isDown("up") then
    if not (self.yPos + delta_yPos > window_height - self.height) then
      self.yPos = self.yPos + delta_yPos
    end
  end
end

function Ship:drawShip(x,y)
  love.graphics.draw(self.graphic,x,y)
end

function Ship:drawChargeGraphic(x,y)
  if self.chargeshot then
    love.graphics.setColor(self.chargeshot.charge_color)
    self.chargeshot.charge_effect.animation:draw(
        self.chargeshot.charge_effect.graphic,
        x - self.chargeshot.charge_effect.width * 0.5,
        y - self.chargeshot.charge_effect.height * 0.5)
    love.graphics.setColor(255, 255, 255)
  end
end

function Ship:drawHitbox()
  if self.hitbox then
  love.graphics.setColor(0, 255, 0)
  self.hitbox:draw('fill')
  love.graphics.setColor(255, 255, 255)
  end
end

function Ship:update(dt)
  self:move(dt)
  self:moveHitbox(dt)
  self:charging(dt)
  return self:fire(dt), self:fireChargeShot(dt)
end

function Ship:draw(xOffset,yOffset)
  local xPos = self.xPos + xOffset
  local yPos = self.yPos + yOffset
  self:drawChargeGraphic(xPos + self.width * 0.5, yPos + self.height * 0.5)
  self:drawShip(xPos,yPos)
  -- self:drawHitbox()
end
