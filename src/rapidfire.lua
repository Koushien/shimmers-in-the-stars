local shapes = require 'include/HardonCollider.shapes'

require 'src/bullet'

RapidFire = {}
RapidFire.__index = RapidFire

setmetatable(RapidFire, {
  __index = Bullet,
  __call = function(cls, x, y)
    local self = setmetatable({}, cls)
    self:init(x, y)
    return self
  end
})

RapidFire.graphic = love.graphics.newImage("res/space-shooter/shots/4.png")
RapidFire.width, RapidFire.height = RapidFire.graphic:getDimensions()
RapidFire.yVel = -414
RapidFire.damage = 1
RapidFire.range = 488

function RapidFire:init(x, y)
  Bullet.init(self)
  self.xPos, self.yPos = x - self.width * 0.5 , y
  self.hitbox =
    shapes.newCircleShape(self.xPos + self.width * 0.5,
      self.yPos + self.height * 0.5, 5)
end
