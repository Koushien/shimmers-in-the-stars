-- A charge shot is a bullet with levels ranging from 1 to 3.
local anim8 = require 'include/anim8/anim8'
local shapes = require 'include/HardonCollider.shapes'

require 'src/bullet'

ChargeShot = {}
ChargeShot.__index = ChargeShot

setmetatable(ChargeShot, {__index = Bullet})

ChargeShot = {
  -- level one
  {
    graphic = love.graphics.newImage("res/space-shooter/shots/4.png"),
    yVel = -212,
    damage = 1,
    range = 244,
    level = 1,
    charge_graphic = nil,
    charge_color = {255, 234, 150, 200}
  },
  -- level two
  {
    graphic = love.graphics.newImage("res/space-shooter/shots/9.png"),
    yVel = -281,
    damage = 8,
    range = 374,
    level = 2,
    charge_color = {255, 46, 71, 200}
  },
  -- level three
  {
    graphic = love.graphics.newImage("res/space-shooter/shots/8.png"),
    yVel = -563,
    damage = 30,
    range = 486,
    level = 3,
    charge_color = {151, 241, 193, 200}
  },
}
ChargeShot.charge_effect = {
  graphic = love.graphics.newImage("res/space-shooter/effects/fx-1.png"),
  width = 38, height = 38
}
ChargeShot.charge_effect.grid = anim8.newGrid(
  38, 38, ChargeShot.charge_effect.graphic:getDimensions())
ChargeShot.charge_effect.animation =
  anim8.newAnimation(ChargeShot.charge_effect.grid('6-1',1), 0.10)

ChargeShot[1].__index = ChargeShot[1]
ChargeShot[2].__index = ChargeShot[2]
ChargeShot[3].__index = ChargeShot[3]

local chargeshot_metatable = {
  __index = Bullet,
  __call = function(cls, x, y)
    local self = setmetatable({}, cls)
    ChargeShot.init(self, cls, x, y)
    return self
  end
}

setmetatable(ChargeShot[1], chargeshot_metatable)
setmetatable(ChargeShot[2], chargeshot_metatable)
setmetatable(ChargeShot[3], chargeshot_metatable)

ChargeShot[1].width,ChargeShot[1].height = ChargeShot[1].graphic:getDimensions()
ChargeShot[1].hitbox = function(x, y)
  local hitbox = shapes.newCircleShape(
      ChargeShot[1].width * 0.5, ChargeShot[1].height * 0.5, 5)
  hitbox:move(x, y)
  return hitbox
end

ChargeShot[2].width, ChargeShot[2].height =
    ChargeShot[2].graphic:getDimensions()
ChargeShot[2].hitbox = function(x, y)
  -- start from top to bottom, left to right
  local hitbox = shapes.newPolygonShape(
      ChargeShot[2].width * 0.5, 0,
      0, ChargeShot[2].height,
      ChargeShot[2].width, ChargeShot[2].height)
  hitbox:move(x, y)
  return hitbox
end

ChargeShot[3].width, ChargeShot[3].height =
    ChargeShot[3].graphic:getDimensions()
ChargeShot[3].hitbox = function(x, y)
 -- start from top to bottom, left to right
 local hitbox = shapes.newPolygonShape(
    0, 0,
    ChargeShot[3].width, 0,
    ChargeShot[3].width * 0.5, ChargeShot[3].height)
  hitbox:move(x, y)
  return hitbox
end

function ChargeShot:init(base, x, y)
  Bullet.init(self)
  self.xPos, self.yPos = x - base.width * 0.5, y
  self.hitbox = base.hitbox(self.xPos, self.yPos)
  self.charge_effect = base.charge_effect
  self.charge_color = base.charge_color
end
