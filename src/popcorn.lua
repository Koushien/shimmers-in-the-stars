-- Basic enemies that make up most formations the players face.
local shapes = require 'include/HardonCollider.shapes'

require 'src/enemy'

Popcorn = {}
Popcorn.__index = Popcorn

setmetatable(Popcorn, {__index = Enemy})

Popcorn = {
  graphic = love.graphics.newImage("res/space-shooter/ships/10.png"),
  xVel = 300,
  yVel = 300,
  alpha = love.graphics.newImage("res/space-shooter/ships/10a.png"),
  small = {
    health = 3,
    scale = 1
  },
  medium = {
    health = 8,
    scale = 1.15
  },
  large = {
    health = 15,
    scale = 1.3
  }
}
Popcorn.width, Popcorn.height = Popcorn.graphic:getDimensions()

Popcorn.hitbox = function(x, y, scale)
  local hitbox = shapes.newCircleShape(
    Popcorn.width * 0.5 * scale, Popcorn.height * 0.5 * scale, 10 * scale)
  hitbox:move(x, y)
  return hitbox
end

Popcorn['small'].__index = Popcorn['small']
Popcorn['medium'].__index = Popcorn['medium']
Popcorn['large'].__index = Popcorn['large']

local popcorn_metatable = {
  __index = setmetatable(Popcorn, {__index = Enemy}),
  __call = function(cls, x, y)
    local self = setmetatable({}, cls)
    Popcorn.init(self, cls, x, y)
    return self
  end
}

setmetatable(Popcorn['small'], popcorn_metatable)
setmetatable(Popcorn['medium'], popcorn_metatable)
setmetatable(Popcorn['large'], popcorn_metatable)

function Popcorn:init(base, x, y)
  Enemy.init(self)
  self.health = base.health
  self.width, self.height = self.width * base.scale, self.height * base.scale
  self.xPos = (x or 0) - self.width * 0.5
  self.yPos = (y or 0) - self.width * 0.5
  self.hitbox = Popcorn.hitbox(self.xPos, self.yPos, base.scale)
end
