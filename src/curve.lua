require 'include/math/math'

Curve = {}
Curve.__index = Curve

setmetatable(Curve, {
  __call = function(cls, vertices)
    local self = setmetatable({}, cls)
    self:init(vertices)
    return self
  end
})

function arc_length(points)
  local arc_length = 0
  local i = 1
  while i + 3 < #points do
    arc_length = arc_length + math.dist(points[i], points[i+1],
                                        points[i+2], points[i+3])
    i = i + 2
  end
  return arc_length
end

function Curve:init(vertices)
  self.curve = love.math.newBezierCurve(vertices)
  self.points = self.curve:render(self.curve:getControlPointCount())
  self.arc_length = arc_length(self.points)
  self.cur_pos = 0
end

function Curve:nextPoint(delta)
  self.cur_pos = math.clamp(self.cur_pos + (delta / self.arc_length), 0, 1)
  return self.cur_pos, self.curve:evaluate(self.cur_pos)
end

function Curve:getSpeed(t)
  local vel = (self.curve:getSegment(0, t)):getDerivative()
  local deg_vel = vel:getDegree()
  local norm_vel = 0

  for i = 1, deg_vel do
    local x0, y0 = vel:getControlPoint(i)
    local x1, y1 = vel:getControlPoint(i + 1)
    local dist = math.sqrt((x1 - x0) ^ 2 + (y1 - y0) ^ 2)
    norm_vel = norm_vel + dist
  end
  return norm_vel
end
