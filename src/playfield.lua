-- Playfields consist of all the moving parts of the screen: its respective
-- player, enemies and bullets. It is in charge of collision checking its own.
local HC = require 'include/HardonCollider'
local dlist = require 'include/dlist/dlist'

require 'src/bag'
require 'src/bullet'
require 'src/enemy'
require 'src/player'
require 'src/starfield'

Playfield = {}
Playfield.__index = Playfield

setmetatable(Playfield, {
  __call = function(cls)
  local self = setmetatable({}, cls)
  self:init()
  return self
  end,
})

function Playfield:init()
  self.player = nil
  self.bag = Bag()
  self.collider = HC.new()
  self.bullets = dlist:new()
  self.enemies = dlist:new()
  self.starfield = Starfield()
end

function Playfield:setPlayer(player)
  self.player = player
  self.collider:register(player.ship.hitbox)
end

function Playfield:addBullet(bullet)
  self.collider:register(bullet.hitbox)
  self.bullets:push(bullet)
end

function Playfield:addEnemy(enemy)
  self.collider:register(enemy.hitbox)
  self.enemies:push(enemy)
end

function Playfield:updatePlayer(dt)
  return self.player:update(dt)
end

function Playfield:updateBag(dt)
  incoming = self.bag:tick(dt)
  if incoming ~= nil then
    for enemy in incoming:items() do
      self:addEnemy(enemy)
    end
  end
end

function Playfield:updateBullets(dt)
  for bullet in self.bullets:items() do
    if bullet.alive == false then
      self.bullets:remove(bullet)
    else bullet:update(dt)
    end
  end
end

function Playfield:updateEnemies(dt)
  for enemy in self.enemies:items() do
    if enemy.alive == false then
      if enemy.explosion then
        self:addBullet(enemy:explode())
      end
      self.enemies:remove(enemy)
    else enemy:update(dt)
    end
  end
end

function Playfield:checkCollisions(dt)
  for enemy in self.enemies:items() do
    local candidates = self.collider:neighbors(enemy.hitbox)
    for bullet in self.bullets:items() do
      for candidate in pairs(candidates) do
        local collides, _, _ = enemy.hitbox:collidesWith(candidate)
        if collides and self.player.hitbox == candidate then
          self.player:collidesWithEnemy()
        end
        if collides and bullet.hitbox == candidate then
          enemy.collidesWith.bullet(enemy, bullet:damage())
          bullet.collidesWith.enemy(bullet, enemy)
        end
      end
    end
  end
end

function Playfield:drawPlayer(xOffset,yOffset)
  self.player:draw(xOffset,yOffset)
end

function Playfield:drawBullets(xOffset,yOffset)
  for bullet in self.bullets:items() do
    xOffset, yOffset = xOffset or 0, yOffset or 0
    bullet:draw(xOffset,yOffset)
  end
end

function Playfield:drawEnemies(xOffset,yOffset)
  for enemy in self.enemies:items() do
    xOffset, yOffset = xOffset or 0, yOffset or 0
    enemy:draw(xOffset,yOffset)
  end
end

function Playfield:update(dt)
  local rapidfire, chargeshot = self:updatePlayer(dt)
  if rapidfire then self:addBullet(rapidfire) end
  if chargeshot then self:addBullet(chargeshot) end
  self:updateBag(dt)
  self:updateBullets(dt)
  self:updateEnemies(dt)
  self:checkCollisions(dt)
  self.starfield:update(dt)
end

function Playfield:draw(xOffset,yOffset)
  self.starfield:draw()
  self:drawPlayer(xOffset,yOffset)
  self:drawBullets(xOffset,yOffset)
  self:drawEnemies(xOffset,yOffset)
end
