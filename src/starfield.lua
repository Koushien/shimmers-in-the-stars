-- Class for creating a scrolling stream of stars. On initialization, it should
-- pre-populate a playfield. Afterwards, it more-or-less randomly emits new
-- stars falling down the screen.
Starfield = {}
Starfield.__index = Starfield

setmetatable(Starfield, {
  __call = function(cls)
    local self = setmetatable({}, cls)
    self:init()
    return self
  end
})

Starfield.img = love.graphics.newImage('res/space-shooter/effects/fx-5.png')
Starfield.star = love.graphics.newQuad(
    28, 0, 14, 28, Starfield.img:getDimensions())

function Starfield:initCanvases()
  self.small = love.graphics.newCanvas(1,1)
  love.graphics.setCanvas(self.small)
    love.graphics.clear()
    love.graphics.points(0.5, 0.5)
  love.graphics.setCanvas()

  self.medium = love.graphics.newCanvas(3.5, 7)
  love.graphics.setCanvas(self.medium)
    love.graphics.clear()
    love.graphics.draw(Starfield.img, Starfield.star, 0, 0, 0, .25, .25)
  love.graphics.setCanvas()

  self.large = love.graphics.newCanvas(7, 14)
  love.graphics.setCanvas(self.large)
    love.graphics.clear()
    love.graphics.draw(Starfield.img, Starfield.star, 0, 0, 0, .5, .5)
  love.graphics.setCanvas()
end

function Starfield:initParticleSystems()
  local small = love.graphics.newParticleSystem(self.small, 1024)
  small:setParticleLifetime((love.graphics.getHeight() + 20) / 50);
  small:setLinearAcceleration(0, 50, 0, 100)

  local medium = love.graphics.newParticleSystem(self.medium, 128)
  medium:setParticleLifetime(love.graphics.getHeight() / 225);
  medium:setLinearAcceleration(0, 225, 0, 450)

  local large = love.graphics.newParticleSystem(self.large, 128)
  large:setParticleLifetime(love.graphics.getHeight() / 425);
  large:setLinearAcceleration(0, 425, 0, 850)

  table.insert(self.psystems, small)
  table.insert(self.psystems, small)
  table.insert(self.psystems, medium)
  table.insert(self.psystems, large)
end

function Starfield:prepopulate()
  local width, height = love.graphics.getDimensions()
  for i = 1, #self.psystems do
    for _ = 1, 64 do
      self.psystems[i]:moveTo(math.random(0, width * 0.5), math.random(0, height))
      self.psystems[i]:emit(1)
    end
    self.psystems[i]:moveTo(0, 0)
  end
end

function Starfield:init()
 self.psystems = {}
 self:initCanvases()
 self:initParticleSystems()
 self:prepopulate()
end

function Starfield:update(dt)
  local width = love.graphics.getWidth()
  for i = 1, #self.psystems do
    self.psystems[i]:setEmissionRate(math.random(25, 35))
    self.psystems[i]:moveTo(math.random(0, width * 0.5), -20)
    self.psystems[i]:update(dt)
  end
end

function Starfield:draw()
  for i = 1, #self.psystems do
    love.graphics.draw(self.psystems[i], 0, 0)
  end
end
