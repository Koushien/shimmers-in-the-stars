require 'src/ship'

Player = {}
Player.__index = Player

setmetatable(Player, {
  __call = function(cls)
  local self = setmetatable({}, cls)
  self:init()
  return self
  end
})

function Player:init()
  self.ship = nil
end

function Player:setShip(ship)
  self.ship = ship
end

function Player:update(dt)
  return self.ship:update(dt)
end

function Player:draw(xOffset,yOffset)
  self.ship:draw(xOffset,yOffset)
end
