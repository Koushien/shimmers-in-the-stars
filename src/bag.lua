-- Store all patterns of a certain tier in a table and call this a bag
-- Pick a random pattern from this bag, making sure it is not the
-- previously selected one.
require 'src/patterns'

Bag = {}
Bag.__index = Bag

setmetatable(Bag, {
  __call = function(cls)
    local self = setmetatable({}, cls)
    self:init()
    return self
  end
})

function Bag:init()
  self.patterns = Patterns()
  self.previous = nil
  self.cooldown = 6.5
end

function Bag:grab()
  return self.patterns[love.math.random(1, #self.patterns)]()
end

function Bag:tick(dt)
  self.cooldown =  self.cooldown + dt
  if self.cooldown >= 4 then
    self.cooldown = 0
    return self:grab()
  end
  return nil
end
