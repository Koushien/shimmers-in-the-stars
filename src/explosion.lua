local shapes = require 'include/HardonCollider.shapes'
local anim8 = require 'include/anim8/anim8'

require 'src/bullet'

Explosion = {}
Explosion.__index = Explosion

setmetatable(Explosion, {
  __index = Bullet,
  __call = function(cls, x, y, scale)
    local self = setmetatable({}, cls)
    self:init(x, y, scale)
    return self
  end
})

local function add(table, key)
  table[key] = true
end

Explosion.graphic = love.graphics.newImage("res/space-shooter/effects/fx-2.png")
Explosion.grid = anim8.newGrid(34,34, Explosion.graphic:getDimensions())
Explosion.width, Explosion.height = 34, 34
Explosion.damage = 3
Explosion.range = 1

Explosion.collidesWith = {
  enemy = function(self, enemy) add(self.previously_collided, enemy) end
}

function Explosion:init(x, y, scale)
  Bullet.init(self)
  self.previously_collided = {}
  self.scale = scale
  self.width, self.height = self.scale / self.width, self.scale / self.height
  self.xPos, self.yPos = x + self.width * 0.5, y - self.height * 0.5
  local radius = scale / 2
  self.hitbox = shapes.newCircleShape(self.xPos + scale * 0.5,
                                      self.yPos + scale * 0.5,
                                      radius)
  function self:damage()
    return function(enemy)
      if self.previously_collided[enemy] == nil then
      enemy.health = enemy.health - Explosion.damage
      end
    end
  end
  self.animation = anim8.newAnimation(
    self.grid('1-5',1), 0.1,
    function() self.animation:pauseAtEnd() self.alive = false end)
end

function Explosion:finish()
  self.animation:pauseAtEnd()
  self.alive = false
end

function Explosion:update(dt)
  self.animation:update(dt)
end

function Explosion:draw(xOffset,yOffset)
  self.animation:draw(self.graphic, self.xPos + xOffset, self.yPos + yOffset, 0,
                      self.width, self.height)
  if self.hitbox then
    love.graphics.setColor(0,255,255,100)
    local function draw_hitbox() self.hitbox:draw('fill') end draw_hitbox()
    love.graphics.setColor(255,255,255)
  end
end
