-- Patterns coordinate the enemies to be spawned, their initial coordinates and
-- their movement throughout the pattern.
dlist = require 'include/dlist/dlist'

require 'src/curve'
require 'src/enemy'
require 'src/popcorn'

Pattern = {}
Pattern.__index = Pattern

local px_to_unit = {
  width = function(px, padding)
    return (px - padding) / love.graphics.getWidth() * 2 + padding
  end,
  height = function(px, padding)
    return (px - padding) / love.graphics.getHeight() + padding
  end
}

local unit_to_px = {
  -- [0, 1] : [0 + self.width, love.graphics.getWidth() / 2 - self.width]
  width = function(unit, padding)
    return (unit * love.graphics.getWidth() / 2 - (1.5 * padding)) + padding
  end,
  -- [0, 1] : [0 + self.height, love.graphics.getHeight() - self.height]
  height = function(unit, padding)
    return (unit * love.graphics.getHeight() - (1.5 * padding)) + padding
  end
}

function Pattern.init(self, template)
  local instructed_enemies = dlist:new()
  for i=1,#template do
    for j=1,#(template[i].enemies) do
      current_enemy = template[i].enemies[j]()
      local k = 1
      local instructions = {}
      while k < #template[i].instructions do
        instructions[k]= unit_to_px.width(template[i].instructions[k],
                                           current_enemy.width)
        instructions[k+1] = unit_to_px.height(template[i].instructions[k+1],
                                              current_enemy.height)
        k = k + 2
      end
      table.insert(instructions, 1, current_enemy.xPos)
      table.insert(instructions, 2, current_enemy.yPos)
      current_enemy.curve = Curve(instructions)
      instructed_enemies:push(current_enemy)
    end
  end
  self.enemies = instructed_enemies
end
