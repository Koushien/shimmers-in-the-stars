-- A gamescreen is composed of an order of playfields that it can display in the
-- end user's preferred order.
require 'src/playfield'

Gamescreen = {}
Gamescreen.__index = Gamescreen

setmetatable(Gamescreen, {
  __call = function(cls)
    local self = setmetatable({}, cls)
    self:init()
    return self
  end
})

function Gamescreen:init()
  self.playfields = {}
end

function Gamescreen:updatePlayfields(dt)
  for i=1,#self.playfields do
    self.playfields[i]:update(dt)
  end
end

function Gamescreen:setPlayfield(playfield, num)
  self.playfields[num] = playfield
end

function Gamescreen:update(dt)
  self:updatePlayfields(dt)
end

function Gamescreen:draw()
  local window_width, window_height = love.graphics.getDimensions()
  local window_horizontal_center = window_width * 0.5

  -- vline
  love.graphics.line(window_horizontal_center, 0,
                     window_horizontal_center, window_height)

  for i=1,#self.playfields do
    -- p1 has no offset
    -- p2 only needs a xOffset starting at midscreen
    local xOffset = (i - 1 % 2) * window_horizontal_center
    local yOffset = math.floor(i / 2) * window_height
    love.graphics.setScissor(xOffset, yOffset,
                             window_width * 0.5, window_height)
    self.playfields[i]:draw(xOffset, yOffset)
    love.graphics.setScissor()
  end
end
